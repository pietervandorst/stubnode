'use strict';

import {Response} from './Response';
import {Request} from "./Request";

export class Stub {
    constructor(public request: Request, public response: Response) {
    }
}