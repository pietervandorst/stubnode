'use strict';
import {Stub} from "../stub/Stub";

export class Matchers {

    public matchHttpMethod(stub:Stub, method:string):Boolean {
        console.log('attempting to match ' + method + ' to: ' + stub.request.method);
        return stub.request.method.toString() == method;
    }

    public matchHttpHeaders(stub:Stub, headers:{[key:string]:string}):Boolean {
        var match:boolean = true;

        if (stub.request.headers != null) {
            for (let key in stub.request.headers) {
                console.log(key + ' should be inside header string: ' + headers.toString());
                match = headers[key] != undefined
            }
        }

        return match;
    }

    public matchPath(stub:Stub, path:string):Boolean {
        console.log('Attempting to match: ' + stub.request.path + ' to: ' + path);
        return stub.request.path === path;
    }

    public matchBody(stub:Stub, body:string):Boolean {
        console.log(body)
        if (body.length != 0 && stub.request.method != 'GET') {
            JSON.parse(body, function (key, value) {
                console.log(key);
                console.log(value);
            });

        }

        return true;
    }
}